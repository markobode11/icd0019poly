package poly.customer;

import java.util.Optional;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";

    public Optional<AbstractCustomer> getCustomerById(String id) {
        throw new RuntimeException("not implemented yet");
    }

    public void remove(String id) {
        throw new RuntimeException("not implemented yet");
    }

    public void save(AbstractCustomer customer) {
        throw new RuntimeException("not implemented yet");
    }

    public int getCustomerCount() {
        throw new RuntimeException("not implemented yet");
    }
}
